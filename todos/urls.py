from django.urls import path
from todos.views import (
    todo_list,
    todo_list_detail,
    create_todo,
    update_todo_list,
    delete_todo,
    create_todo_item,
    update_todo_item,
)

urlpatterns = [
    path("items/<int:id>/edit/", update_todo_item, name="todo_item_update"),
    path("items/create/", create_todo_item, name="create_todo_item"),
    path("create/", create_todo, name="create_todo"),
    path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/edit/", update_todo_list, name="todo_list_update"),
    path("<int:id>/delete/", delete_todo, name="delete_todo"),
]
