from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, ItemForm


# Create your views here.
def todo_list(request):
    todo_lists = TodoList.objects.all()
    context = {
        "todo_list": todo_lists,
    }
    return render(request, "todos/todo_list.html", context)


def todo_list_detail(request, id):
    item = get_object_or_404(TodoList, id=id)
    context = {"todo_items": item}
    return render(request, "todos/todo_list_detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoForm()

    context = {
        "form": form,
    }
    return render(request, "todos/todo_create.html", context)


def update_todo_list(request, id):
    update = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=update)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=update)
    context = {"form": form}
    return render(request, "todos/todo_list_update.html", context)


def delete_todo(request, id):
    todelete = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todelete.delete()
        return redirect("todo_list_list")
    context = {"form": todelete}
    return render(request, "todos/todo_delete.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            form.save()
            todo_list = form.cleaned_data["list"]
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = ItemForm()

    context = {
        "item_form": form,
    }

    return render(request, "todos/todo_create_item.html", context)


def update_todo_item(request, id):
    itemupdate = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=itemupdate)
        if form.is_valid():
            form.save()
            todo_list = form.cleaned_data["list"]
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = ItemForm(instance=itemupdate)

    context = {"item_form": form}

    return render(request, "todos/todo_item_update.html", context)
